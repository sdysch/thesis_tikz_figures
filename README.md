# thesis_tikz_figures

Tikz diagrams I made for my thesis

![pp collision](figures/parton_shower.png)
![tau_had jet](figures/tauJetCartoon.png)
![proton](figures/proton.png)
![SSB in ferromagnet](figures/magnet.png)
